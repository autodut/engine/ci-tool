<!-- This content is under a '## Development notes' block //-->

This release notes file resides in root folder and is expected to be emptied after a release, so each new release gets a new release content.

This file is processed with `envsubst` so any environment variable will be replaced. If this is correctly performed, `CI_COMMIT_TAG` should be a valid tag string.

> CI_COMMIT_TAG: ${CI_COMMIT_TAG}

Peace

## Modules checklist

<!-- Use :green_circle:, :orange_circle: or :red_circle: at cells //-->

| :bookmark_tabs:  | base         | semver       | gitflow      | gitlab       |
| :-----:          | :-----:      | :-----:      | :-----:      | :-----:      |
| Dependencies     | :red_circle: | :red_circle: | :red_circle: | :red_circle: |
| Documentation    | :red_circle: | :red_circle: | :red_circle: | :red_circle: |
| Base variables   | :red_circle: | :red_circle: | :red_circle: | :red_circle: |
| Module variables | :red_circle: | :red_circle: | :red_circle: | :red_circle: |

<details>
<summary>:mag_right: <strong>Sections detailing each checklist</strong></summary>

### Module level: Dependencies

Check that blocks like the following are present:

```bash
# Required dependencies
if [ -z "${AD_CI_SEMVER_REGEX}" ]
then
  echo "Invalid AD_CI_SEMVER_REGEX variable."
  echo "Please include module.semver.sh before this file"
  exit 1
fi
```

### Module level: Documentation

- Remove duplicate information from comments
- Ensure the information is inside the function so it is located with `!usage`

### Module level: Variables

- Globally defined -> UPPERCASE
- `local`-defined -> lowercase
- Template:

```
#!/usr/bin/env bash
[License block]
# Take a look to README.md to get function template and tips

# First the path, so we can source anything before setting our variables
AD_CI_TOOL_PATH="$(dirname "$(readlink -f "$0")")"

# Base variables
AD_CI_NAME="NAME-OF-THE-FILE.sh"
AD_CI_DESCRIPTION="DESCRIPTION"
AD_CI_META="INTRODUCED AT YEAR"
AD_CI_AUTHOR="AUTHOR"

# Define allowed commands
AD_CI_COMMANDS=( \
  "commands-here" \
)
```

### Function level: Documentation

- Remove duplicate information from comments
- Ensure the information is inside the function so it is located with `!usage`

### Function level: Variables

- Globally defined -> UPPERCASE
- `local`-defined -> lowercase

</details>

## Generated badges list

| `pass`          | `warning`          | `fail`          | `info`          | `b0b000`          |
| :-----:         | :-----:            | :-----:         | :-----:         | :-----:           |
| ![Badge pass][] | ![Badge warning][] | ![Badge fail][] | ![Badge info][] | ![Badge b0b000][] |

[Badge pass]:    ${AD_CI_ARTIFACTS_URL}/badge-pass.svg?job=release
[Badge warning]: ${AD_CI_ARTIFACTS_URL}/badge-warning.svg?job=release
[Badge fail]:    ${AD_CI_ARTIFACTS_URL}/badge-fail.svg?job=release
[Badge info]:    ${AD_CI_ARTIFACTS_URL}/badge-info.svg?job=release
[Badge b0b000]:  ${AD_CI_ARTIFACTS_URL}/badge-b0b000.svg?job=release
