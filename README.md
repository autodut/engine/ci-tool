# CI Tool

This repository contains a set of bash scripts and its auxiliary files to ease common tasks performed both by user during development, or by runners during CI/CD. It can be used not only for software-oriented projects, but also documentation and hardware.

- SW/FW: *Demos to be defined here*
    + High-level (C, Node, ...)
    + Low-level (ARM C, FreeRTOS, ...)
- HW: *Demos to be defined here*
    + Altium
    + LTSpice
- Doc: *Demos to be defined here*
    + User documentation (PDF/Docx)
    + Automated documentation (PDF/Docx)
    + VuePress for project self-doc

It is designed with the following goals in mind:

1. **Modular**: Group of related commands are divided into modules that can be added or removed at will.
2. **Portable**: A set of rules are strictly applied when coding the files so the final code can be run in a wide group of shells.
3. **Scalable**: Different projects can import this one in different ways, then make it grow by just adding more commands or modules while taking advantage of all functionality already available.

To continue reading, please use the following table of content:

- [CI Tool documentation](./docs/README.md)
    + [Installation](./docs/install.md)
    + [Module development](./docs/module-development.md)
    + [Notes about GitLab cache and artifacts](./docs/gitlab-cache-and-artifact.md)

## Copyright and License

**TL;DR**

```
 MIT License
 
 Copyright (c) 2020 CieNTi <cienti@cienti.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
```

To read the *full text* please refer to the [LICENSE file](./LICENSE)
