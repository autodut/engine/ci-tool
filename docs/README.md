# CI Tool documentation

## Install

Go to [Install](./install.md)

## Branching strategy and Versioning

We use [Semantic Versioning](http://semver.org/) rules to compose a valid version string. Available versions can be found as [tags on this repository](-/tags).

For the branching strategy, we opted for [GitFlow](https://support.gitkraken.com/git-workflows-and-extensions/git-flow/) as it is widely used and correctly mimics semantic version stages.

If a different flow is required, please stick to the version scheme and review CI steps to accomodate any required changes.
