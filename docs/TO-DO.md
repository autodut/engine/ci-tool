# TO-DO

## To be documented

### Config flags

**Where to put this information?**

---

Flag: `AD_CI_D_ECHO_SHOW_APPNAME`

- Default: undefined
    + Do not print app name when using `d_echo`
    + Pros/Reasons: Cleaner output
    + Cons: Tracking message app is not possible if a big mixed log
- Accepted: '1'
    + Print app name when using `d_echo`
    + Pros: Full trackability
    + Cons: Dirtier/More dense output

*Output when **disabled***

```
[01:52:57](build_git-changelog) Content has been saved as:
[01:52:57](build_git-changelog)   /home/cienti/code/autodut/engine/ci-tool/build/build_git-changelog.md
```

*Output when **enabled***

```
[01:54:17][test_ci-tool](build_git-changelog) Content has been saved as:
[01:54:17][test_ci-tool](build_git-changelog)   /home/cienti/code/autodut/engine/ci-tool/build/build_git-changelog.md
```

---

Flag: `AD_CI_COLORS`

- Default: undefined
    + Do not print colors
    + Reason: Full compatibility
- Accepted: '1'
    + Print with ANSI colors
    + Pros: Console-compatible (not all)
    + Cons: Weird characters if seen from inside an incompatible console or a document
