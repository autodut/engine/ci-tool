# Notes about GitLab cache and artifacts files

**TL;DR;**

- **Cache content** is meant to be reused from job to job as a way to reduce pipeline time.
    This content is ensured between jobs only if there is cache availability, and it will depend on the runner.
- **Artifact content** is meant to be released/published as a part of the pipeline flow.
    This content is always downloaded from previous stages before the job is executed, so it is always available. In difference with cache content, artifacts are not shared from pipeline to pipeline, while cache can (but again, is not ensured unless specific configuration is ensured).

For more information, keep reading :D

## Cache

Depending how it is configured, it will be shared across:

- All jobs of a pipeline
- Same job of different pipelines
- All jobs of all pipelines
- ...

Two common flows are described:

- Same nature jobs:
    + Tipically a job-per-stage that mimics the user shell session operations
- Different nature jobs:
    + Expected to be executed in different architectures (x86, x64, arm, ...)
    + Depends on different sources or toolchains (C, Node, Python, ...)

Each flow requires different use of both cache and artifact features.

## Artifacts

Artifacts are only shared between a pipeline jobs by GitLab design, so a job can be feeded from a previous job artifact, but not from a previous pipeline artifact.

Any out-of-standard requirement (like to need a previous pipeline artifact) needs to be performed using the API or an alternate/not-documented method.

## Cache conflict between pipelines

In order to avoid conflicts and colissions between different branches, pipelines and jobs, we assume the worst case:

- Pipelines are running in parallel
- Sharing the same runner
- Runner has enough parallel slots to launch everything at the same time

In this case, a classic step like `clean` and generate some mess, as same stage + job in different pipelines will save to the same cache in different moments, so final cache will be the last one to be written.

This can easily be avoided by using a cache key based, at least, on the branch name. This way, no simultaneous pipelines which requires the same cache will be fired again.

## `No URL provided` related messages

If debugging a job, any of this messages can be read:

- No URL provided, cache will not be downloaded from shared cache server. Instead a local version of cache will be extracted.
- No URL provided, cache will be not uploaded to shared cache server. Cache will be stored only locally.

This is something related to the runner configuration, and means that there is no configured [distributed cache](https://docs.gitlab.com/runner/configuration/autoscale.html#distributed-runners-caching).

In practice this means that if a different runner is needed or used in a different job, cache will not be available and/or it will mismatch.

## Example 1: Same nature jobs

In this case we want to share some folders and/or files across the pipeline stages and jobs, while keeping strong isolation with another pipeline environment.

To make 100% safe, it is also recommended to tag both the runner and the jobs, so same machine and environment is used across the pipeline. By design, this will also ensure the cache availability.

```yaml
stages:
  - prepare
  - build
  - release

variables:
  AD_CI_CACHE_DIR: "ci_cache"

Prepare cache folder:
  stage: prepare
  before_script:
    - mkdir -p ${AD_CI_CACHE_DIR}
  script:
    - tree > ${AD_CI_CACHE_DIR}/tree-output_prepare
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - ${AD_CI_CACHE_DIR}/

Create some extra content:
  stage: build
  before_script:
    - mkdir -p ${AD_CI_CACHE_DIR}
  script:
    - tree > ${AD_CI_CACHE_DIR}/tree-output_build
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - ${AD_CI_CACHE_DIR}/

Release some of cached content as artifacts:
  stage: release
  before_script:
    - mkdir -p ${AD_CI_CACHE_DIR}
  script:
    - tree > ${AD_CI_CACHE_DIR}/tree-output_release
  after_script:
    - cp ${AD_CI_CACHE_DIR}/tree-output_* .
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - ${AD_CI_CACHE_DIR}/
  artifacts:
    paths:
      - tree-output_*
    exclude:
      - ${AD_CI_CACHE_DIR}/*
    name: "${CI_PROJECT_NAME}_${CI_COMMIT_SHORT_SHA}_${CI_COMMIT_REF_SLUG}"
    untracked: true
```
