# How-To: Bash

This document aims to **not** cover what is bash or how to use it, if that is what you want, please head to:

- [Bash Reference Manual](https://www.gnu.org/software/bash/manual/html_node/index.html)

And now that you know about it, let's going to learn how to make bash to do nice things robustly and safely.

## The *Shebang*: Executable script vs. Library

First of all, the very first line a bash script have to have to avoid headaches is the [Shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)), which tells the system which interpreter to use to run the script.

Second, regardless of the file name, the script file needs the execution bit (+x) enabled. Exceptions may apply here, like when the script is called as an argument to the bash itself, but in this section it is assumed the script is executed by using a direct call, which is the common approach.

### Portability problems

The main problem of using a shebang is that the full path of the interpreter have to be hardcoded, so a simple `bash` call (relying in the system to correctly find the bash executable) is simply not possible. So the first shebang one may find, is one of the most common ones:

```bash
#!/bin/bash
```

Nowadays, with this line the script will work in (almost, let the doubt always be) all Linux systems. But Linux is not BSD or Mac, or *put-your-special-case-here* and we can not ensure the location of the bash executable everywhere.

### The 'most portable' way

The requirement of a full path for the shebang will always be a portability problem, but it is also possible to narrow as much as possible the fail cases, by using the following shebang instead:

```bash
#!/usr/bin/env bash
```

Will it be available **always** on **all** systems? No, but this way is the closest one to that level of availability.

- **PRO:** It is possible to use custom versions of bash. If that is what you want, just add your custom version path to `PATH` environment before the system one. The script will then use the custom version with no more modifications.
- **CON:** It is possible to use custom versions of bash. If you are not aware of a custom change to the PATH, and the script was planned with a specific version in mind, the script execution may fail.

### Executable vs. Library convention

In this context, an executable will contain code that is planned to be executed when the script is called, where a library will contain code that will do nothing if the script is called. This is only true if inside the library are only variables or functions definitions, and not direct calls.

Here arises the first problem: There is not a standard way to differentiate a script that is planned to be an executable, from a second one that is supposed to be a library. And there is also no control for a library to be called as an executable.

And that is why *the convention is strongly recommended*:

| Property       | Executable script | Library script |
| :-----         | :-----:           | :-----:        |
| File extension | None              | `.sh`          |
| Executable bit | Enabled           | Disabled       |
| Shebang        | Always            | None/Special   |

With this light convention:

- The script is executable because the execution bit, which means it is fully self-defined (no matter the extension, or the lack of it) and the system will have no problems with it.
- The library at a first stage already informs about its nature because the extension, and it also try to block an unintended direct execution with no execution bit and none or special shebang.

The *Special* shebang is one extra level of robustness to avoid direct calls to libraries. As being said, the shebang contains the full path to the executable that will interpret the script, but what if the shebang is something like the following?

```bash
#!/usr/bin/printf ERROR: This file is a library. Exitting now ..\n
```

This way, the system will use `/usr/bin/printf` as the interpreter if a library has the execution bit enabled. Because `printf` nature, a message will be printed to stdout before exit, with no further script execution. So if directly called:

```bash
> $ ./my-library.sh
ERROR: This file is a library. Exitting now .. ./my-library.sh
```

The last step to finish the special shebang is to hide the `./my-library.sh ` part, which is appended by the system when calling the interpreter:

```bash
#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
```

So we finish with:

```bash
> $ ./my-library.sh
ERROR: This file is a library. Exitting now ..
```

### Troubleshooting

If after all this barriers are set and met, there is still failures:

- The user made something wrong: Review shebang and script execution bit
- The system does not have `/usr/bin/env`: Seriously, review your system, then follow the next step.
- You have a very special case, tell us about your history!

## Write safe code

Unlike C, Bash is quite flexible/dynamic/hackable, which is really nice if you can control each aspect of the execution, but is not always easy or under-your-full-control.

**First scenario:** There are sometimes a bash script is failing, and that hidden character (or the lack of it) that makes no syntax error and is also invisible for your eyes, is driving the script to finish with no errors, but it is also behaving not-as-expected. But before to try to figure it now, we need to enter a different scenario.

**A different scenario:** Where you have a fully working script, but you need to copy and paste into an outside tool, like `.gitlab-ci.yml`. There is a high chance that the script will fail without further tweaks. Ok .. *WHY?*.

When we write a CI job with a script block, we can end with something like:

```yaml
My nice job name that can contain spaces freely:
  stage: test
  script:
    - first-command
    - export SOME_VAR="with some value"
    - echo "And now we can use $SOME_VAR"
```

The job:

- Begins by calling `first-command`, which is available and valid
- Export to the environment a new variable
- A message is printed using `SOME_VAR` (which demonstrates how the script keep environment defined between each steps)
- The job, as there was no error involved, finish with a "Pass"

Under-the-hood:

GitLab CI conform a shell call composed like `bash -c "first-command" -c "export .." -c ..`.

> The first disadvantage of this method is that you need to prefix a script with '-' TO BE CONTINUED!

## Avoid `set -e`/`set +e` at all cost!

It may appear a Panacea, and it tried to, but is far to have that honor.

Instead of reinventing explanations, here we have a nice *Copy & Paste* of [this Gist](https://gist.github.com/dsharp-pivotal/758af6dc88c50e49d3091f85dcdc6504):

> A bash puzzle for you. For each, ask yourself:
> Does the shell exit? What gets printed?
> 
> Common setup:
> 
> set -e
> foo() { false; echo hello ; }
> 
> 1) true && foo && true && echo "nope"
> 2) true && foo ; echo "hmm"
> 
> Now it gets extra weird:
> 3) foo | cat
> 4) foo && true
> 
> Answers:
> (1) prints "hello" and "nope", and the shell does not exit.
> (2) prints nothing, and the shell exits.
> (3) prints nothing, and the shell does not exit.
> (4) prints "hello", and the shell does not exit.
> 
> What's going on? Why does "hello" get printed in (1), but not (2)? Why
> doesn't it print in (3)
> 
> One way to explain it is that "set -e" means that the shell will exit
> for the first "uncaught" error. An error can be "caught" by &&, ||, |,
> if, until, when, for, and similar compound command constructs. Or, in
> other words, &&, ||, |, etc, will effectively "disable" set -e...
> unless it's the last item in a list of &&/||, or the last item in a
> pipeline.
> 
> So it seems that a function knows that its outer context will catch
> its errors or not. If so, then a non-zero status will not cause it to
> return early or exit the shell.
> 
> This means that with set -e, the behavior of a function depends on how
> it is called (or how it's callers are called). Weird.
> 
> Take this as a reminder not to do "real programming" in bash. And
> don't rely on set -e to catch errors deep in your code.

## Why `local var="$(invalid-command) || return $?;"` throws no error?

As happens in other languages, in *Bash* there is a way to define variables with limited visibility/availability to the function where they resides, and this is performed by `local` keyword.

On the other hand, we can (and we must) control the exit status of a command, so we know if it behaved good or bad. We already know that we will can not use `set +e`/`set -e`, but what we can use instead?

The first, is what we learned by reading ~stackoverflow~ [Bash Reference Manual](https://www.gnu.org/software/bash/manual/html_node/index.html):

```bash
# Are you optimist?
if (good-command);
then
  echo "I'm good, thanks!";
  # Infinite numbers for infinite errors sources ... but only the 0 is the king
  exit 0;
fi;

# Or pessimist?
if (! good-command);
then
  echo "I failed! Let me alone!";
  # O boi!
  exit 1;
fi;
```

TO-BE-CONTINUED
