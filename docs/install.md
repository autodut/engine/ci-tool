# Installation

## Prerequisites and dependencies

It will only apply to the runners in charge of carrying each job, as the commands will be required there.

Because that, and taking into mind that different jobs can be executed in different runners, it is needed to create a valid approach where there is low to no chance to finish in a failed pipeline due to not found commands.

> *GitLab specific*: One recommendation is to tag both the runners and the jobs, so each job is now locked (and ensured) to a valid environment. *NOTE: As a continuation to this feature, it is recommended to [read about the caching](./gitlab-cache-and-artifact.md)*.

In all the cases where a shell is involved it is expected a Bash or compatible one, so please ensure its availability. If will be called by `/usr/bin/env bash`, so if a compatible shell creates a valid symlink, it is supposed to also works.

- **module.core.sh**: Core functionality that can be used virtually anywhere.
    + `entrypoint()`: bash shell
    + `minify-bash-script()`: bash shell, `sed`
- **module.semver.sh**: Semantic Version 2 helpers
    + `semver-test()`: bash shell
- **module.gitflow.sh**: Git Flow specific functionality
    + `feature-to-release()`: bash shell, `git`
- **module.gitlab.sh**: GitLab specific functionality
    + `create-release-json()`: `jq`, `envsubst (gettext package)`
- **module.misc.sh**: Pure GitLab-oriented functionality
    + `build_git-changelog()`: bash shell, `git`
    + `build_release-notes()`: bash shell, `envsubst (gettext package)`
    + `build_badge()`: bash shell, `envsubst (gettext package)`
    + `build_vuepress()`: bash shell, `yarn`
- **ci-tool**: Wrapper for `entrypoint()` where source code is included.
    + As it is a wrapper for all required `source` calls plus the real call to `entrypoint()`, it will depends on the same as its source files.
- **test_ci-tool**: Same tasks as `ci-tool`, but with extra debug information.

## Flavours

*CI Tool* has been developed to be used in three different scenarios:

### Hard copy

This is the easiest way to use it:

- Pros:
    + Once copied inside your project, it will simply work
    + It is always present in the folder structure
    + Works in offline scenarios where a remote repository is not reachable
- Cons:
    + Out-of-sync: Is the user task to review for new features and/or hotfixes

1. Grab the distributable archive from [URL NEEDED HERE!](URL NEEDED HERE!)
2. Extract it into a repository folder  
    Recommended: `<repo>/.ci/ci-tool` so the top-level script is reached as `/usr/bin/env bash .ci/ci-tool/ci-tool`. This way `.ci` folder, commonly used by the community, is not messed with all *CI Tool* files.
3. Use it for common tasks or write a CI file with it :D

### Submodule

This is the recommended approach when user environment is bash-compatible and any of the required functionalities or commands are available.

- Pros:
    + Same as hard copy, once git submodules are downloaded, it will simply work
    + Easy to realize when a file modification must have been made in a different file: As soon as the submodule present changes to be pushed, you should check and fix your approach :D
    + Easy to get updates: If submodule is tied to a latest instead to a fixed version, as soon as the submodule present changes to be pulled, there is something new and can be grabbed easily.
- Cons:
    + Will not work in offline scenarios if not previously pulled/downloaded

1. Select which version to use
2. Add its url as submodule:
    - If submodule is residing in the same server, please use relative path instead full url
    - If submodule is outside the server, then use full url
3. Synchronize everything and go!

### Docker

This is one alternative method to work using *CI Tool* when there is no bash-compatible capabilities and is recommended if the proposed base images fit your needs.

- Pros:
    + Works everywhere where Docker works, with all the pros & cons it inherit
    + Easy to integrate from/to a huge amount of places, like GitLab
- Cons:
    + Will not work in offline scenarios if not previously pulled/downloaded
    + If the provided images base image does not fit your needs, you may require to build your own image before to be useful for you

TO-BE-CONTINUED
