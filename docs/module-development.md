# Module development

A *CI Tool* module is a bash script file that follows the following rules:

1. Have to be written so it can be used inline (remove all comments and newlines) and as safe as possible regarding the handling of text variables (spaces, newlines ...).  
    This is the most portable way to write it and can be directly imported into GitLab `script` blocks without extra failure.  
    A way to test that it is correctly written is to minify it (see core module `minify-bash-script` command and `test_ci-tool` file for more information .. and test it!)
2. Add required information to *Base variables*  
    ```bash
    # Base variables
    AD_CI_NAME=( "module.example.sh" );
    AD_CI_DESCRIPTION=( "Example module brief explanation" );
    AD_CI_SEMVER=( "1.0.0" );
    AD_CI_META=( "2020" );
    AD_CI_AUTHOR=( "Author <em@il.com>" );
    # Define allowed commands
    AD_CI_COMMANDS=( "example-command" );
    ```
3. Code a command following the proposed command API

## *CI Tool* Commands API

### Mandatory variables

A command is expected to be self-documented in a way that both user and developer can use it without text redundancy (that ends in incomplete/unmaintained documentation).

Due to bash nature, function/command arguments should not be too much complex, but due to the "self-documenting" CI Tool nature, it has to be able to receive complex data.

As a compromise solution, a few variables are always expected at the begining of a command before of calling `print_usage_and_help` as shown:

```bash
example-command()
{
  # Common stage: Description, help and usage block
  local _desc=( \
    "Main description line. Shown at top-level '-h' and '-c', and command '-h'" \
    "2nd line shown only at command '-h'" \
    "3rd line shown only at command '-h'" \
    "4th line shown only at command '-h'" \
    "..." \
  );
  local _usage="[options] c_arg"
  local _getopts=":ab:"
  local _optdesc=( \
    "-a" "Default value for a" \
      "Option with no arguments (no ':'). Tipically used for On/Off flags." \
    "-b <b_arg>" "Default value of <b_arg>" \
      "Option with argument (with ':'). Used to pass data to command." \
    "<c_arg>" "Default value of <c_arg>" \
      "Command argument, out of options. Tipically used for mandatory data." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;
  ...
```

After this block, we're sure that any documentation is put in the right place and a basic option + argument parsing has been done (like intercepting the common '-h', that is the reason no command has it defined inside).

### Recommended guidelines

From this point, is up to the user to decide how to continue, but the following guidelines are recommended (and strictly applied in the provided files):

1. Define defaults for options and arguments before the mandatory block, so no duplicity is needed for documentation + usage:  
    ```bash
    example-command()
    {
      # Variables related to options. Set all defaults here
      local a_flag="No";
      local b_option="some default option value";
      local c_arg="some default argument value";
    
      # Common stage: Description, help and usage block
      ...
      local _optdesc=( \
        "-a" "${a_flag}" \
          ...
        "-b <b_arg>" "${b_option}" \
          ...
        "<c_arg>" "${c_arg}" \
          ...
      );
      ...
    
      # Specific stage: Option parsing
      local opt="";
      local OPTIND=0;
      while getopts "${_getopts}" opt;
      do
        case ${opt} in
          "a") a_flag="Yes"; ;;
          "b") b_option="${OPTARG}"; ;;
        esac;
      done;
      shift "$((OPTIND-1))";
      ...
    
      # Specific stage: Arguments parsing
      local c_arg="${1}";
      if ([ "${c_arg}" = "" ]);
      then
        echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Empty argument not allowed.";
        return 1;
      fi;
      ...
    ```
2. Use auxiliar functions for normalized test + messages  
    By following the previous steps almost all auxiliar function has been touched at least once, but they are internal to the user for clarity except for `print_usage_and_help` (used before) and `path_not_found`, described here:  
    The following code block will check if a path exists in form of file or folder.  
    ```bash
      # Specific stage: Path checks
      if (path_not_found "${some_file}"); then return 1; fi;
    ```
    If path is valid, it will just continue with no extra messages, while if not, it will print a normalized message, where both script and command names are pre-filled, like the following:  
    ```
    ci-tool build_badge: Path not found '/some/invalid/path'.
    ```

TO-BE-CONTINUED
