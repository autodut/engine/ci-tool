#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Base variables
AD_CI_NAME=( "core.sh" );
AD_CI_DESCRIPTION=( "CI Tool core functionality" );
AD_CI_SEMVER=( "1.0.0" );
AD_CI_META=( "2020" );
AD_CI_AUTHOR=( "CieNTi <cienti@cienti.com>" );

# Define allowed commands
AD_CI_COMMANDS=( \
  "minify-bash-script" \
  "semver-test" \
);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# There is only one variable that is mandatory: AD_CI_TOOL_PATH
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if ([ "${AD_CI_TOOL_PATH}" = "" ] || [ ! -d "${AD_CI_TOOL_PATH}" ]);
then
  echo "${0} (${AD_CI_NAME}): ERROR! Invalid AD_CI_TOOL_PATH";
  echo "  - Current value .: '${AD_CI_TOOL_PATH}'";
  exit 1;
fi;

# Define build path for generated content
AD_CI_BUILD_PATH=${AD_CI_BUILD_PATH:-"${AD_CI_TOOL_PATH}/build"};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: minify-bash-script
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
minify-bash-script()
{
  # Variables related to options. Set all defaults here
  local out_file="stdout";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Minify a bash script by stripping comments and newlines." \
  );
  local _usage="[options] <in_file>";
  # Enabled options: Use ":" if no options, ":x" for flags, ":x:" for arguments
  local _getopts=":o:";
  # Options triplet: "format" "default value" "description"
  # Default value:
  #   - "::none::" .....: Remove 'Default: val' line for that option/argument
  #   - "::required::" .: Print a common 'Mandatory argument ...'
  #   - Other values ...: Print the value as 'Default: value' line
  local _optdesc=( \
    "-o <out_file>" "${out_file}" \
      "Place the minified output into <out_file> instead of stdout." \
    "<in_file>" "::required::"
      "Read data to minify from <in_file>." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "o") out_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Other arguments parsing
  local in_file="${1}";
  if ([ "${in_file}" = "" ] || [ ! -f "${in_file}" ]);
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Empty or invalid input file.";
    return 1;
  fi;

  # Compose sed sequence
  local sed_sequence="";
  # Remove comments
  sed_sequence="${sed_sequence} /^[\t\ ]*#/d;";
  # Remove empty lines
  sed_sequence="${sed_sequence}/^$/d;";
  # Remove leading and trailing whitespaces
  sed_sequence="${sed_sequence}s/^[ \t]*//;s/[ \t]*$//;";
  # Remove trailing escape character
  sed_sequence="${sed_sequence}s/\\\\[ \t]*$//;";
  # Remove newlines (not working yet, using tr for now)
  # sed_sequence="${sed_sequence}:a;N;$!ba;s/\n/ /g;";
  
  # Create final output content
  local out_content="$(sed -e "${sed_sequence}" ${1} | tr "\n" " ")";

  # Save to stdout or file
  if ([ "${out_file}" = "stdout" ]);
  then
    echo "${out_content}";
  else
    echo "${out_content}" > "${out_file}";
  fi;

  # Return gracefully
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: semver-test
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
semver-test()
{
  # Variables related to options. Set all defaults here
  local semver_prefix="";
  local show_regex="No";
  local be_verbose="No";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Check if the passed argument is Project + Semantic Version 2 compliant."
    "SemVer2-compliant strings are never prefixed by anything but the MAJOR,"
    "but it is common to prefix by an 'v' in scenarios like repository tags,"
    "or variable names."
  );
  local _usage="[options] <version_string>";
  local _getopts=":p:rv";
  local _optdesc=( \
    "-p <semver_prefix>" "${semver_prefix}" \
      "Value set as <semver_prefix> will be allowed before a SemVer2 string." \
    "-r" "${show_regex}" \
      "Show regular expression applied to <version_string>." \
    "-v" "${be_verbose}" \
      "Show a verbose dissection of <version_string>, if valid." \
    "<version_string>" "::required::" \
      "String to test against Project + Semantic Version 2 rules." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "p") semver_prefix="${OPTARG}"; ;;
      "r") show_regex="Yes"; ;;
      "v") be_verbose="Yes"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Other arguments parsing
  local version_string="${1}";
  if ([ "${version_string}" = "" ]);
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Empty version string not allowed.";
    return 1;
  fi;

  # Compose SemVer2 regex by portions:
  # NOTE:
  #   A prefix is never used to compose a version according semver2, but in
  #   order to avoid possible errors (starting with a number can generate
  #   problems), a prefix is used. It defaults to "v" for readability.
  ## Has to start with the prefix or it will fail
  _rgx="^${semver_prefix}";
  ## Has to contain the Major part
  _rgx="${_rgx}(0|[1-9][0-9]*)\\.";
  ## Has to contain the Minor part
  _rgx="${_rgx}(0|[1-9][0-9]*)\\.";
  ## Has to contain the Patch part
  _rgx="${_rgx}(0|[1-9][0-9]*)";
  ## Optional to have prerelease string
  _rgx="${_rgx}(-((0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*)";
  _rgx="${_rgx}(\\.(0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*))*))?";
  ## Optional to have metadata string
  _rgx="${_rgx}(\\+([0-9a-zA-Z-]+(\\.[0-9a-zA-Z-]+)*))?$";
  ## Finish the regex
  AD_CI_SEMVER_REGEX="${_rgx}(\\+([0-9a-zA-Z-]+(\\.[0-9a-zA-Z-]+)*))?$";

  # Print the regular expression?
  if ([ "${show_regex}" = "Yes" ]);
  then
    echo "Showing applied regex to match against Semantic Version 2 string:";
    echo "${AD_CI_SEMVER_REGEX}";
  fi;

  # Check format
  if [[ "${version_string}" =~ ${AD_CI_SEMVER_REGEX} ]];
  then
    if ([ "${be_verbose}" = "Yes" ]);
    then
      echo "Release tag match Project + Semantic Version 2 rules:";
      echo "  - Valid tag for release: ${version_string}";
      echo "    + Prefix (out of semver) ..: '${semver_prefix}'";
      echo "    + Major ...................: ${BASH_REMATCH[1]}";
      echo "    + Minor ...................: ${BASH_REMATCH[2]}";
      echo "    + Patch ...................: ${BASH_REMATCH[3]}";
      echo "    + Pre-release .............: ${BASH_REMATCH[5]}";
      echo "    + Metadata ................: ${BASH_REMATCH[10]}";
    fi;
    # Return gracefully
    return 0;
  else
    if ([ "${be_verbose}" = "Yes" ]);
    then
      echo "Release tag does not match Project + Semantic Version 2 rules";
      echo "  - Prefix (out of semver) ..: '${semver_prefix}'";
    fi;
    # Return with error
    return 1;
  fi;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: *_<color>
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Helper to easily write ANSI color codes
# 
# Arguments:
#   - ${1}..$N..: Text to be printed in color
# 
# Note:
#   'n' stands for 'Normal'
#   'b' stands for 'Bold'
#   Enable color support by setting AD_CI_COLORS to "On"
if ([ "${AD_CI_COLORS}" = "1" ]);
then
  n_black()   { echo -e "\e[30m${@:1}\e[0m"; };
  n_red()     { echo -e "\e[31m${@:1}\e[0m"; };
  n_green()   { echo -e "\e[32m${@:1}\e[0m"; };
  n_yellow()  { echo -e "\e[33m${@:1}\e[0m"; };
  n_blue()    { echo -e "\e[34m${@:1}\e[0m"; };
  n_magenta() { echo -e "\e[35m${@:1}\e[0m"; };
  n_cyan()    { echo -e "\e[36m${@:1}\e[0m"; };
  n_white()   { echo -e "\e[37m${@:1}\e[0m"; };
  b_black()   { echo -e "\e[90m${@:1}\e[0m"; };
  b_red()     { echo -e "\e[91m${@:1}\e[0m"; };
  b_green()   { echo -e "\e[92m${@:1}\e[0m"; };
  b_yellow()  { echo -e "\e[93m${@:1}\e[0m"; };
  b_blue()    { echo -e "\e[94m${@:1}\e[0m"; };
  b_magenta() { echo -e "\e[95m${@:1}\e[0m"; };
  b_cyan()    { echo -e "\e[96m${@:1}\e[0m"; };
  b_white()   { echo -e "\e[97m${@:1}\e[0m"; };
else
  n_black()   { echo "${@:1}"; };
  n_red()     { echo "${@:1}"; };
  n_green()   { echo "${@:1}"; };
  n_yellow()  { echo "${@:1}"; };
  n_blue()    { echo "${@:1}"; };
  n_magenta() { echo "${@:1}"; };
  n_cyan()    { echo "${@:1}"; };
  n_white()   { echo "${@:1}"; };
  b_black()   { echo "${@:1}"; };
  b_red()     { echo "${@:1}"; };
  b_green()   { echo "${@:1}"; };
  b_yellow()  { echo "${@:1}"; };
  b_blue()    { echo "${@:1}"; };
  b_magenta() { echo "${@:1}"; };
  b_cyan()    { echo "${@:1}"; };
  b_white()   { echo "${@:1}"; };
fi;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: d_echo
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Print a pre-formatted debug message to stdout
# 
# Arguments:
# - $1 ..: Message to print
# - $2 ..: Optional. Caller text to print
# - $3 ..: Optional. Appname to print
d_echo()
{
  local _date="[$(b_black "$(date +%H:%M:%S)")]";
  local _caller="($(n_white "${2:-"${FUNCNAME[1]}"}"))";
  if ([ "${AD_CI_D_ECHO_SHOW_APPNAME}" = "1" ]);
  then
    local _appname="[$(b_black "${3:-"${AD_CI_APPNAME}"}")]";
  else
    local _appname="";
  fi;

  echo "${_date}${_appname}${_caller} ${1}";
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: e_echo
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Print a pre-formatted error message to stdout and exit with a code
# 
# Arguments:
# - $1 ..: Error code to use for exit
# - $2 ..: Message to print
# - $3 ..: Optional. Caller text to print
# - $4 ..: Optional. Appname to print
e_echo()
{
  local _date="[$(b_black "$(date +%H:%M:%S)")]";
  local _caller="($(n_white "${3:-"${FUNCNAME[1]}"}"))";
  if ([ "${AD_CI_D_ECHO_SHOW_APPNAME}" = "1" ]);
  then
    local _appname="[$(b_black "${4:-"${AD_CI_APPNAME}"}")]";
  else
    local _appname="";
  fi;

  echo "${_date}${_appname}${_caller} $(n_red "ERROR.${1}:") $(b_red "${2}")";
  exit ${1};
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: path_not_found
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Check if a file or folder exists and print normalized error message if not
#   -
#   WARNING about return codes
#     This specific function return inverted values than usual, being 1 if OK,
#     and 0 when there is something to regret of.
#   -
# 
# Arguments:
# - $1 ..: Path to check
path_not_found()
{
  # If exists, is file or folder, exit because everything are rainbows
  if ([ -e "${1}" ] || [ -f "${1}" ] || [ -d "${1}" ]);
  then
    return 1;
  fi;

  # No rainbows for you!
  d_echo "$(b_red "Path not found") '${1}'." "${FUNCNAME[1]}";
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: print_opt_usage
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Print command argument option with normalized format
# 
# Arguments:
# - $1 ..: Option format
# - $2...: Default value: "::required::", "::none::" or some value
# - $3...: Description
print_opt_usage()
{
  # Print mandatory 'information' line
  printf "  %-19.*s %s\n" ${#1} "${1}" "${3}";

  # Compose optional 'default value' line
  local default_value="${2}";
  if ([ "${default_value}" = "::required::" ]);
  then
    default_value="Mandatory: User value is needed or execution will fail.";
  elif ([ ! "${default_value}" = "::none::" ]);
  then
    default_value="Default value: '${default_value}'.";
  fi;

  # Print optional 'default value' line
  if ([ ! "${default_value}" = "::none::" ]);
  then
    printf "%22s${default_value}\n" "";
  fi;
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: print_usage_and_help
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Intercept the command options, check for invalid ones, and print usage or
#   help if required, otherwise it does nothing but returning.
#   This allows easier conditionals and better semantics for the function name.
#   To use this behaviour, define all the 4 required variables (see depends),
#   then include a line like:
#            if (print_usage_and_help "${@:1}"); then return 1; fi;
#   After it, we are sure there is a valid option, or the command is compliant
#   with what is expected.
#   -
#   WARNING about return codes
#     This specific function return inverted values than usual, being 1 if OK,
#     and 0 when there is something to regret of.
#   -
#   Before any operation is performed, a pre-command is called, so user will be
#   able to execute previous tasks common to all commands with the environment
#   of the specific command. With this method it is also possible to stop the
#   command, by just exiting with failure (return 1).
#   --
#   This pre-command function is defined here as a dummy function. User only
#   needs to re-define somewhere and it will be working with no extra warnings.
#   It is expected standard return codes (0:OK, 1:Error)
    __pre_command() { return 0; };
#   --
# 
# Arguments:
# - $1..$N .: Command arguments
# 
# Depends on:
#   - ${_desc}    -
#   - ${_usage}    |_ All this variables must be defined before the call
#   - ${_getopts}  |
#   - ${_optdesc} -
print_usage_and_help()
{
  # Pre-command
  if (! __pre_command "${@:1}"); then { return 0; } fi;

  # Usage and argument information
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}0h" opt;
  do
    case ${opt} in
      # Special command, used from entrypoint to request a description
      "0")
        echo "${_desc}";
        return 0;
      ;;
      # Print usage & help
      "h"|"?")
        if ([ "${opt}" = "?" ]);
        then
          d_echo "$(b_red "Unrecognized option") '${1}'" "${FUNCNAME[1]}";
        fi;
        echo "Description:";
        for desc_idx in ${!_desc[@]};
        do
          echo "  ${_desc[${desc_idx}]}";
        done;
        echo "Usage:";
        echo "  ${AD_CI_APPNAME} ${FUNCNAME[1]} ${_usage}";
        echo "Options:";
        print_opt_usage "-h" "::none::" "Print this message and exit.";
        # Iterate each pair of opt + desc
        for ((idx=0; idx<${#_optdesc[@]}; idx=idx+3));
        do
          print_opt_usage "${_optdesc[idx]}" \
                          "${_optdesc[idx+1]}" \
                          "${_optdesc[idx+2]}";
        done;
        return 0;
      ;;
    esac;
  done;
  shift "$((OPTIND-1))";
  return 1;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Function: entrypoint
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description:
#   Check for command, validate it and execute it returning success or fail
# 
# Arguments:
# - $1 .....: Command
# - $2..$N .: Command arguments
# 
# Depends on:
#   - ${AD_CI_NAME}
#   - ${AD_CI_DESCRIPTION}
#   - ${AD_CI_COMMANDS}
entrypoint()
{
  #              $--------------------------------} = String of the last one
  #                          [-------------------]  = Index of the last one
  #                           $---------------}     = Element count
  AD_CI_APPNAME="${AD_CI_NAME[${#AD_CI_NAME[@]}-1]}";
  local invalid_option=0;
  local req_help=0;
  local req_mi=0;
  local req_ci=0;
  local valid_command=0;
  local this_cmd="";

  # Core argument management
  local opt="";
  local OPTIND=0;
  while getopts ":mch" opt;
  do
    case ${opt} in
      "?")
        echo "${AD_CI_APPNAME}: Unrecognized option '${1}'";
        invalid_option=1;
        req_help=1;
      ;;
      "m") req_mi=1; ;;
      "c") req_ci=1; ;;
      "h") req_help=1; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  if ([ "${req_mi}" = "1" ]);
  then
    # Print modules information
    echo "# Modules";
    echo "";
    echo "Information about modules loaded by \`${AD_CI_APPNAME}\`.";
    echo "";
    for mi_idx in "${!AD_CI_NAME[@]}";
    do
      echo "## \`${AD_CI_NAME[${mi_idx}]}\`";
      echo "";
      echo " - Description: **${AD_CI_DESCRIPTION[${mi_idx}]}**";
      echo " - Semver: **${AD_CI_SEMVER[${mi_idx}]}**";
      echo " - Author: **${AD_CI_AUTHOR[${mi_idx}]}**";
      echo " - Meta: **${AD_CI_META[${mi_idx}]}**";
      echo "";
    done;
    return 0;
  fi;

  if ([ "${req_ci}" = "1" ]);
  then
    # Print commands information
    echo "# Commands";
    echo "";
    echo "Information about available commands for \`${AD_CI_APPNAME}\`.";
    echo "";
    for mi_idx in "${!AD_CI_COMMANDS[@]}";
    do
      echo "## \`${AD_CI_COMMANDS[${mi_idx}]}\`";
      echo "";
      echo "\`\`\`";
      entrypoint "${AD_CI_COMMANDS[${mi_idx}]}" -h;
      echo "\`\`\`";
      echo "";
    done;
    return 0;
  fi;

  # Known command?
  if ([ "$#" -gt "0" ] && [ "${req_help}" = "0" ]);
  then
    for this_cmd in "${AD_CI_COMMANDS[@]}";
    do
      if [ "${this_cmd}" = "${1}" ];
      then
        valid_command=1;
        break;
      fi;
    done;

    # Invalid command?
    if ([ "${valid_command}" = "0" ]);
    then
      echo "${AD_CI_APPNAME}: Unrecognized command '${1}'";
      req_help=1;
    fi;
  fi;

  # Empty command?
  if ([ "${this_cmd}" = "" ] && [ "${req_help}" = "0" ]);
  then
    echo "${AD_CI_APPNAME}: Empty command not allowed";
    invalid_option=1;
    req_help=1;
  fi;

  # Help?
  if ([ "${req_help}" = "1" ]);
  then
    echo "Usage:";
    echo "  ${AD_CI_APPNAME} -h";
    echo "  ${AD_CI_APPNAME} command -h";
    echo "  ${AD_CI_APPNAME} command [options] ...";
    echo "Options:";
    print_opt_usage "-h" "::none::" "Print this message and exit.";
    print_opt_usage "-m" "::none::" \
      "Print modules information in Markdown format and exit.";
    print_opt_usage "-c" "::none::" \
      "Print commands information in Markdown format and exit.";
    print_opt_usage "command" "::required::" \
      "Executes a command, tunnelling all the options to it.";
    echo "Commands:";
    for this_cmd in "${AD_CI_COMMANDS[@]}";
    do
      # Use option '-0' to request for a command description
      printf "  %-19.*s %s\n" ${#this_cmd} "${this_cmd}" "$(${this_cmd} -0)";
    done;
  fi;

  # Invalid option?
  if ([ "${invalid_option}" = "1" ] || [ "${valid_command}" = "0" ]);
  then
    # Help is already printed, time to exit with sadness :(
    return 1;
  fi;

  # If we arrived here, we deserve to execute a command
  if ("${@:1}");
  then
    # Command executed successfully
    return 0;
  else
    # Command exited with failure
    return 1;
  fi;
};
