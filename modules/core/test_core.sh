#!/usr/bin/printf ERROR: This file is a test library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Export only a variable containing a list of test cases. This list is composed
# by groups of consecutive values, described as follows:
#   - Test title: Human-readable short test describing the test
#   - Expected result: 0 for success, 1 for error
#   - Command line: Whatever is needed after 'ci-tool' command
export test_cases=( \
  # Calls with no command associated, only options
  "*core:* Empty call" \
    "1" "" \
  "*core:* Help request" \
    "1" "-h" \
  "*core:* Modules information request" \
    "0" "-m" \
  "*core:* Commands information request" \
    "0" "-c" \
  # Command: semver-test
  "*semver-test:* Invalid tag strings (no prefix)" \
    "1" "semver-test -r -v -p v 1.2.3" \
  "*semver-test:* Valid tag strings ('v' prefix + semver2)" \
    "0" "semver-test -r -v -p v v1.2.3" \
  "*semver-test:* Valid tag strings (numbers over 9)" \
    "0" "semver-test -r -v -p x x10.20.30" \
  "*semver-test:* Valid tag strings (no prefix + semver2)" \
    "0" "semver-test -r -v 1.2.3" \
);
