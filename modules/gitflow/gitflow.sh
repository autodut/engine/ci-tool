#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Base variables
AD_CI_NAME+=( "gitflow.sh" );
AD_CI_DESCRIPTION+=( "Execute common GitFlow tasks with a single command" );
AD_CI_SEMVER+=( "1.0.0" );
AD_CI_META+=( "2020" );
AD_CI_AUTHOR+=( "CieNTi <cienti@cienti.com>" );

# Add allowed commands to global list
AD_CI_COMMANDS+=( \
  "feature-to-release" \
);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: feature-to-release
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
feature-to-release()
{
  # Variables related to options. Set all defaults here
  local rel_prefix="";
  
  # Common stage: Description, help and usage block
  local _desc=( \
    "Executes GitFlow feature-to-release procedure on current folder." \
    "It is expected the current folder to be updated with a branch name with" \
    "the form of 'release/<release_tag>'. A sequence of merges will be done" \
    "so a final tag named '<release_tag>' will be created from 'master'." \
  );
  local _usage="[options] <release_tag>";
  local _getopts=":p:";
  local _optdesc=( \
    "-p <rel_prefix>" "${rel_prefix}" \
      "Value set as <rel_prefix> will be allowed before a SemVer2 string." \
    "<release_tag>" "::required::" \
      "Once merged to 'master', create a tag named <release_tag> from it." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "p") rel_prefix="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Other arguments parsing
  local rel_tag="${1}";
  if ([ "${rel_tag}" = "" ]);
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Empty release tag not allowed.";
    return 1;
  fi;

  # Check format
  #if ([[ ! ${rel_tag} =~ ${AD_CI_SEMVER_REGEX} ]]);
  if (! "${0}" semver-test -p "${rel_prefix}" ${rel_tag});
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Invalid release tag '${rel_tag}'.";
    return 1;
  fi;

  local current_branch="$(git rev-parse --abbrev-ref HEAD)";

  # Stash all modified, untracked and ignored files, then clean
  local stash_count_0="$(git stash list | wc -l)";
  git stash push -a -m "Stash created by 'feature-to-release' flow";
  local stash_count_1="$(git stash list | wc -l)";

  # Send feature branch
  git push origin "${current_branch}";

  # Move to develop and merge feature changes
  git checkout develop;
  git merge --no-ff "${current_branch}" \
      -m "Merge branch '${current_branch}' into develop";
  git push origin develop;

  # Create release branch and finish it
  git branch "release/${rel_tag}";
  git checkout master;
  git merge --no-ff "release/${rel_tag}" \
      -m "Merge branch 'release/${rel_tag}'";
  git branch -d "release/${rel_tag}";
  git push origin master;

  # Create tag
  git tag -a "${rel_tag}" \
      -m "$(git log --no-merges --pretty=format:"%s" --max-count=1)";
  git push origin "${rel_tag}";

  # Go back to work
  git checkout "${current_branch}";

  # Recover stash, if present
  if ([ ! "${stash_count_0}" = "${stash_count_1}" ]);
  then
    git stash pop;
  fi;

  # Return gracefully
  return 0;
};
