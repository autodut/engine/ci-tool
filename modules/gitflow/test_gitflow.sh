#!/usr/bin/printf ERROR: This file is a test library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Export only a variable containing a list of test cases. This list is composed
# by groups of consecutive values, described as follows:
#   - Test title: Human-readable short test describing the test
#   - Expected result: 0 for success, 1 for error
#   - Command line: Whatever is needed after 'ci-tool' command
export test_cases=( \
  # Command: feature-to-release
  "*feature-to-release:* Empty call" \
    "1" "feature-to-release" \
  "*feature-to-release:* Help request" \
    "1" "feature-to-release -h" \
  "*feature-to-release:* Invalid release tag ('v' prefix, tag with no prefix)" \
    "1" "feature-to-release -p v 1.2.3" \
  "*feature-to-release:* Invalid release tag (no prefix, tag with 'v' prefix)" \
    "1" "feature-to-release v1.2.3" \
  "*feature-to-release:* Invalid release tag ('v' prefix, tag with 'x' prefix)" \
    "1" "feature-to-release -p v x1.2.3" \
);
