#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Base variables
AD_CI_NAME+=( "gitlab.sh" );
AD_CI_DESCRIPTION+=( "Helper script for GitLab CI/CD purposes" );
AD_CI_SEMVER+=( "1.0.0" );
AD_CI_META+=( "2020" );
AD_CI_AUTHOR+=( "CieNTi <cienti@cienti.com>" );

# Add allowed commands to global list
AD_CI_COMMANDS+=( \
  "create-release-json" \
  "api-create-release" \
);

# Set this module assets folder
AD_CI_GITLAB_ASSET_PATH="${AD_CI_TOOL_PATH}/modules/gitlab/asset";

# It is expected to work under GitLab CI/CD, so this script expects variables
# that are only available under such scenario. But it is also expected to run
# it by hand under own environment.
# 
# In order to be able to test or run this script outside a GitLab CI/CD, the
# required variables are guarded here so, if not defined, they get a value.
export GITLAB_USER_NAME=${GITLAB_USER_NAME:-"CieNTi"};
export CI_API_V4_URL=${CI_API_V4_URL:-"https://gitlab.com/api/v4"};
export CI_COMMIT_SHA=${CI_COMMIT_SHA:-"728d21108432fe24f3831ee3075111794846b371"};
export CI_COMMIT_TAG=${CI_COMMIT_TAG:-"v0.0.0"};
export CI_COMMIT_TIMESTAMP=${CI_COMMIT_TIMESTAMP:-"2020-12-03T07:00:00+02:00"};
export CI_PROJECT_TITLE=${CI_PROJECT_TITLE:-"CI Tool"};
export CI_PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/group/project"};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: create-release-json
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
create-release-json()
{
  # Variables related to options. Set all defaults here
  local description_file="${AD_CI_GITLAB_ASSET_PATH}/release-notes.md";
  local tpl_file="${AD_CI_GITLAB_ASSET_PATH}/gitlab-release-data.json";
  local out_file="${AD_CI_BUILD_PATH}/release-data.json";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Builds a JSON data block valid to create a new GitLab release." \
    "It will follow GitLab releases API (v4)."
  );
  local _usage="[options]";
  local _getopts=":d:t:o:";
  local _optdesc=( \
    "-d <desc_file>" "${description_file}" \
      "Read description content from <desc_file>." \
    "-t <tpl_file>" "${tpl_file}" \
      "Use <tpl_file> as custom template file to compose JSON release data." \
    "-o <out_file>" "${out_file}" \
      "Place the JSON into <out_file>." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "d") description_file="${OPTARG}"; ;;
      "t") tpl_file="${OPTARG}"; ;;
      "o") out_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  local rel_note_desc_json="";

  # Specific stage: Path checks
  if (path_not_found "${description_file}"); then return 1; fi;
  if (path_not_found "${tpl_file}"); then return 1; fi;

  # Convert a markdown text file into JSON string while preserving newlines
  echo "Converting '${description_file}' to JSON compliant string ..";
  rel_note_desc_json="$(jq -Rs '.' ${description_file})" || return $?;

  # Ensure the required variables are correctly exported to envsubst
  export rel_note_desc_json;
  export description_file;
  export CI_PROJECT_TITLE;
  export CI_COMMIT_TAG;
  export CI_COMMIT_SHA;
  export CI_PROJECT_URL;
  # Create final output content
  echo "Create JSON release data from template '${tpl_file}' ..";
  local out_content="";
  out_content="$(envsubst < "${tpl_file}")" || return $?;

  # Save to file
  echo "${out_content}" > "${out_file}";
  echo "JSON file with GitLab release data '${out_file}' saved successfully.";

  # Return gracefully
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: create-release
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
api-create-release()
{
  # Variables related to options. Set all defaults here
  local rel_prefix="";
  local rel_data_json_file="${AD_CI_BUILD_PATH}/release-data.json";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Creates a release from a tag using a description in Markdown format." \
    "It uses GitLab releases API (v4)."
  );
  local _usage="[options] <release_tag>";
  local _getopts=":p:j:";
  local _optdesc=( \
    "-p <rel_prefix>" "${rel_prefix}" \
      "Value set as <rel_prefix> will be allowed before a SemVer2 string." \
    "-j <rel_data_json_file>" "${rel_data_json_file}" \
      "Read release data JSON content from <rel_data_json_file>." \
    "<release_tag>" "::required::" \
      "Once merged to 'master', create a tag named <release_tag> from it." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "p") rel_prefix="${OPTARG}"; ;;
      "j") rel_data_json_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Other arguments parsing
  local release_tag="${1}";
  if ([ "${release_tag}" = "" ]);
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Empty release tag not allowed.";
    return 1;
  fi;

  # Check if tag mets with our tag-version guidelines
  if (! "${0}" semver-test -p "${rel_prefix}" "${CI_COMMIT_TAG}");
  then
    echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Invalid release tag '${rel_tag}'.";
    return 1;
  fi;

  # How to authenticate with GitLab
  #if ...
  #CI_JOB_TOKEN

  # Go for it!
  curl \
    -vvv \
    --trace-ascii - \
    --header "content-type: application/json" \
    --header "job-token: ${CI_JOB_TOKEN}" \
    --fail \
    --show-error \
    --data-binary @"${rel_data_json_file}" \
    --request POST "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases" \
  || return $?;

  # Return gracefully
  return 0;
};
