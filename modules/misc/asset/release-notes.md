# ${CI_PROJECT_TITLE} - Release notes for tag `${CI_COMMIT_TAG}`

- Release author: ${GITLAB_USER_NAME}
- Release date: ${CI_COMMIT_TIMESTAMP}

A common descriptive text to all releases (or changed with low frequency).

## Release relevant links

Tipically permalinks, so also low-frequency changes here

| :linked_paperclips: | A                | B                | C                |
| :-----:             | :-----:          | :-----:          | :-----:          |
| 1                   | [cell A1 link][] | [cell B1 link][] | [cell C1 link][] |
| 2                   | [cell A2 link][] | [cell B2 link][] | [cell C2 link][] |
| 3                   | [cell A3 link][] | [cell B3 link][] | [cell C3 link][] |

[cell A1 link]: ${AD_CI_ARTIFACTS_URL}
[cell B1 link]: ${AD_CI_ARTIFACTS_URL}
[cell C1 link]: ${AD_CI_ARTIFACTS_URL}
[cell A2 link]: ${AD_CI_ARTIFACTS_URL}
[cell B2 link]: ${AD_CI_ARTIFACTS_URL}
[cell C2 link]: ${AD_CI_ARTIFACTS_URL}
[cell A3 link]: ${AD_CI_ARTIFACTS_URL}
[cell B3 link]: ${AD_CI_ARTIFACTS_URL}
[cell C3 link]: ${AD_CI_ARTIFACTS_URL}

<!-- The following variables are replaced by blank text if undefined //-->

${notes_text}

${changelog_text}
