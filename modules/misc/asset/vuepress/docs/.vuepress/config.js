/* Include package.json to avoid variable/data duplications */
const packageConf = require('../../package');

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: packageConf.docs_site.title,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: packageConf.docs_site.description,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#dest
   */
  dest: 'public',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#base
   */
  base: packageConf.docs_site.base_url,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#78ad00' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  markdown: {
    lineNumbers: true,
    /* options for markdown-it-anchor */
    //anchor: { permalink: false },
    /* options for markdown-it-toc */
    toc: { includeLevel: [1, 2] },
    extendMarkdown: md => {
      md.set({ typographer: true }),
      md.use(require('markdown-it-attrs')),
      md.use(require('markdown-it-footnote')),
      md.use(require('markdown-it-include'))
    }
  },

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    /* Navigation bar logo */
    logo: '/vuepress-icon.svg',

    ///* Navigation bar sticky links */
    //nav: [
    //  /* Simple link */
    //  {
    //    text: 'KB',
    //    link: '/kb/',
    //  },
    //  /* Dropdown (allowed more levels) */
    //  {
    //    text: 'Blog',
    //    link: '/blog/'
    //  }
    //],

    /* Navigation bar can be disabled */
    navbar: true,

    /* Sidebars shown by route (frontmatter 'sidebar: auto' overrides it!) */
    sidebar: {
      '/': getSidebarFromPath('Project documentation',
                              packageConf.docs_site.from_folder)
    },

    /* Show h2 (1), h3 (2), ... */
    sidebarDepth: 2,

    /* If false, all other docs are only h1 while current one is expanded */
    displayAllHeaders: false,

    /* Add 'Last Updated' based on git commit date (handled down by plugin) */
    lastUpdated: false,

    /* Next/Prev links */
    nextLinks: true,
    prevLinks: true,

    /* Smooth Scrolling */
    smoothScroll: true,
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    /* Decorate sidebar links when scrolling */
    [ '@vuepress/active-header-links' ],

    /* Loading progress bar */
    [ '@vuepress/nprogress' ],

    /* Reading progress bar (it needs to be after nprogress) */
    [ 'reading-progress' ],

    /* Add an icon once some scroll is done, to easy jump to top */
    [ '@vuepress/plugin-back-to-top' ],

    /* Mimic Medium zoom */
    [ '@vuepress/plugin-medium-zoom', {
      /* See: https://github.com/francoischalifour/medium-zoom#options */
      options: {
        margin: 16
      }
    }],

    /* Configure search based on headers */
    [ '@vuepress/search', {
      searchMaxSuggestions: 10
    }],

    /* Approximate reading time based on document length */
    [ 'reading-time' ],

    /* Replace SearchBox for a full-text search variation */
    [ 'fulltext-search' ],
  ]
}

/* Avoid the need to manually maintain the sidebar list with this function */
function getSidebarFromPath (title, path) {
  var fs = require('fs');
  var files = fs.readdirSync(path);

  /* The empty element represent the index (that is why readme is not added) */
  var list_user_doc = [''];

  /* Add any markdown file (not README) to list so is visible at sidebar */
  var add_ci_tool_doc = false;
  for (var i in files)
  {
    var filename = files[i].split('.').slice(0, -1).join('.');
    var fileext = files[i].split('.').pop();
    
    /* Skip ci-tool folder, enabling flag */
    if (files[i] === "ci-tool")
    {
      add_ci_tool_doc = true;
      continue;
    }

    /* Process user folder */
    if (   (fileext.toLowerCase() === "md")
        && (filename.toLowerCase() !== "readme"))
    {
      list_user_doc.push(filename);
    }
  }

  var output = [{
    title: title,
    collapsable: false,
    children: list_user_doc
  }];

  /* Sould we add ci-tool documentation too? */
  if (add_ci_tool_doc === true)
  {
    var files_ci_tool = fs.readdirSync(path + 'ci-tool/');
    var list_ci_tool_doc = [];
    for (var i in files_ci_tool)
    {
      var filename = files_ci_tool[i].split('.').slice(0, -1).join('.');
      var fileext = files_ci_tool[i].split('.').pop();
      
      /* Process user folder */
      if (fileext.toLowerCase() === "md")
      {
        list_ci_tool_doc.push('ci-tool/' + filename);
      }
    }

    /* Add CI Tool sidebar */
    output.push({
      title: 'CI Tool documentation',
      collapsable: false,
      children: list_ci_tool_doc
    });
  }

  console.log(`${path}: `, output);
  return output;
}
