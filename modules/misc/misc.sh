#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Base variables
AD_CI_NAME+=( "misc.sh" );
AD_CI_DESCRIPTION+=( "Miscellaneous/generic functions for CI/CD tasks" );
AD_CI_SEMVER+=( "1.0.0" );
AD_CI_META+=( "2020" );
AD_CI_AUTHOR+=( "CieNTi <cienti@cienti.com>" );

# Add allowed commands to global list
AD_CI_COMMANDS+=( \
  "build_git-changelog" \
  "build_release-notes" \
  "build_badge" \
  "build_vuepress" \
);

# Set this module assets folder
AD_CI_MISC_ASSET_PATH="${AD_CI_TOOL_PATH}/modules/misc/asset";

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: build_git-changelog
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_git-changelog()
{
  # Variables related to options. Set all defaults here
  local tag_span="5";
  local commit_base_url="${CI_PROJECT_URL}/-/commit/";
  local out_file="${AD_CI_BUILD_PATH}/git-changelog.md";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Create a changelog in Markdown format using commit messages. Based on" \
    "the latest commits found between two tags that are not 'merge' commits." \
  );
  local _usage="[options]";
  local _getopts=":s:u:o:";
  local _optdesc=( \
    "-s <span>" "${tag_span}" \
      "Number of tag span to show." \
    "-u <url>" "${commit_base_url}" \
      "Add <url> prefix for generated commit links (<url>hash)." \
    "-o <out_file>" "${out_file}" \
      "Place the generated content into <out_file>." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "s") tag_span="${OPTARG}"; ;;
      "u") commit_base_url="${OPTARG}"; ;;
      "o") out_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Create final output content
  d_echo "Creating changelog from a span of ${tag_span} tags ..";
  local out_content="";
  local tag_list=();
  tag_list=($(git tag --sort=-v:refname)) || return $?;
  for ((idx=0; idx<${tag_span}; idx++));
  do
    if (   [ ! -z "${tag_list[idx+1]}" ] \
        && [ ! -z "${tag_list[idx]}" ]);
    then
      d_echo "  - Tag '$(b_yellow "${tag_list[idx]}")' found!";
      out_content+="### Tag \`${tag_list[idx]}\`\n";
      out_content+="\n";
      out_content+="$(git log \
                        --no-merges \
                        --pretty=format:"* [%s](${commit_base_url}%H)" \
                        ${tag_list[idx+1]}..${tag_list[idx]})\n";
      out_content+="\n";
    fi;
  done;

  # Save to file
  printf "${out_content}" > "${out_file}";
  d_echo "Content has been saved to:";
  d_echo "  $(n_green "${out_file}")";

  # Return gracefully
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: build_release-notes
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_release-notes()
{
  # Variables related to options. Set all defaults here
  local notes_file="${AD_CI_TOOL_PATH}/NOTES.md";
  local changelog_file="${AD_CI_BUILD_PATH}/git-changelog.md";
  local tpl_file="${AD_CI_MISC_ASSET_PATH}/release-notes.md";
  local out_file="${AD_CI_BUILD_PATH}/release-notes.md";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Create release notes based on a user file, a changelog, and a template." \
    "Environment variables found in notes or in template will be parsed and " \
    "replaced by its value. Changelog will be inserted as-is." \
  );
  local _usage="[options]";
  local _getopts=":n:c:t:o:";
  local _optdesc=( \
    "-n <notes_file>" "${notes_file}" \
      "Notes will be read from <notes_file> file. 'none' to disable." \
    "-c <changelog_file>" "${changelog_file}" \
      "Changelog will be read from <changelog_file> file. 'none' to disable." \
    "-t <tpl_file>" "${tpl_file}" \
      "Use <tpl_file> file to create release notes." \
    "-o <out_file>" "${out_file}" \
      "Place the changelog content into <out_file>." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "n") notes_file="${OPTARG}"; ;;
      "c") changelog_file="${OPTARG}"; ;;
      "t") tpl_file="${OPTARG}"; ;;
      "o") out_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Process notes
  if ([ "${notes_file}" = "none" ]);
  then
    d_echo "Skipping notes integration into release notes ..";
  else
    d_echo "Integrating notes '${notes_file}' into release notes ..";
    if (path_not_found "${notes_file}"); then return 1; fi;
    local notes_text="";
    notes_text="$(envsubst < "${notes_file}")" || return $?;
    # Add final tweak (not big enough for standalone template)
    notes_text="## Notes\n\n${notes_text}\n";
  fi;

  # Process changelog
  if ([ "${changelog_file}" = "none" ]);
  then
    d_echo "Skipping changelog integration into release notes ..";
  else
    d_echo "Integrating changelog '${changelog_file}' into release notes ..";
    if (path_not_found "${changelog_file}"); then return 1; fi;
    local changelog_text="$(cat "${changelog_file}")";
    # Add final tweak (not big enough for standalone template)
    _chg_hd="## Changelog\n\n<details>\n";
    _chg_hd="${_chg_hd}<summary>:mag_right: <strong>Click to open</strong>";
    changelog_text="${_chg_hd}</summary>\n\n${changelog_text}\n</details>\n\n";
  fi;

  # Create final output content
  if (path_not_found "${tpl_file}"); then return 1; fi;
  # Export availability inside envsubst
  export notes_text;
  export changelog_text;
  local out_content="";
  out_content="$(envsubst < "${tpl_file}")" || return $?;

  # Save to file (printf replaces \n without adding an extra newline at the end)
  printf "${out_content}" > "${out_file}";
  d_echo "Content has been saved to:";
  d_echo "  $(n_green "${out_file}")";

  # Return gracefully
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: build_badge
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_badge()
{
  # Variables related to options. Set all defaults here
  local info_text="AutoDUT CI Tool";
  local state_text="Badge";
  local state_color="info";
  local tpl_file="${AD_CI_MISC_ASSET_PATH}/badge_flat-square.svg";
  local out_file="${AD_CI_BUILD_PATH}/badge.svg";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Build a SVG badge. It can be directly used as image in a markdown file." \
  );
  local _usage="[options]";
  local _getopts=":i:s:c:t:o:";
  local _optdesc=( \
    "-i <info_text>" "${info_text}" \
      "Use <info_text> for badge subject text." \
    "-s <state_text>" "${state_text}" \
      "Print <state_text> as badge attention text." \
    "-c <state_color>" "${state_color}" \
      "Use <state_color>. Use pass, warning, fail, info or HEX (no '#')" \
    "-t <tpl_file>" "${tpl_file}" \
      "Use <tpl_file> file to build the badge content." \
    "-o <out_file>" "${out_file}" \
      "Place the badge into <out_file>." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "i") info_text="${OPTARG}"; ;;
      "s") state_text="${OPTARG}"; ;;
      "c") state_color="${OPTARG}"; ;;
      "t") tpl_file="${OPTARG}"; ;;
      "o") out_file="${OPTARG}"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Path checks
  if (path_not_found "${tpl_file}"); then return 1; fi;

  # Nice color palete (from: https://htmlcolorcodes.com/color-picker/)
  # ff5733, #ffbd33, #dbff33, #75ff33, #33ff57, #33ffbd
  local state_color_hex="";
  if ([ "${state_color}" = "pass" ]);
  then
    state_color_hex="#75ff33";
  elif ([ "${state_color}" = "warning" ]);
  then
    state_color_hex="#ffbd33";
  elif ([ "${state_color}" = "fail" ]);
  then
    state_color_hex="#ff5733";
  elif ([ "${state_color}" = "info" ]);
  then
    state_color_hex="#33ffbd";
  else
    state_color_hex="#${state_color}";
  fi;

  # Create final output content
  export info_text;
  export state_text;
  export state_color_hex;
  local out_content="$(envsubst < "${tpl_file}")";

  # Save to file (printf replaces \n without adding an extra newline at the end)
  printf "${out_content}" > "${out_file}";
  d_echo "Content has been saved to:";
  d_echo "  $(n_green "${out_file}")";

  # Return gracefully
  return 0;
};

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Command: build_vuepress
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_vuepress()
{
  # Variables related to options. Set all defaults here
  local docs_folder="docs";
  local tpl_folder="${AD_CI_MISC_ASSET_PATH}/vuepress";
  local out_folder="build/vuepress";
  local full_clean="No";
  local serve_site="No";
  local include_ci_tool_docs="No";

  # Common stage: Description, help and usage block
  local _desc=( \
    "Build a VuePress documentation site using repository and CI Tool docs." \
    "A fully VuePress-compliant folder structure will be created, and filled" \
    "with repository ('docs/' folder) and CI Tool ('-m' option) information." \
    "Build process is launched and site will be browsable once is finished." \
  );
  local _usage="[options]";
  local _getopts=":d:t:o:csi";
  local _optdesc=( \
    "-d <docs_folder>" "${docs_folder}" \
      "Use <docs_folder> to grab the repository/user documentation from." \
    "-t <tpl_folder>" "${tpl_folder}" \
      "Use VuePress <tpl_folder> structure to build the documentation site." \
    "-o <out_folder>" "${out_folder}" \
      "Place the generated documentation site into <out_folder>." \
    "-c" "${full_clean}" \
      "Do a full clean before building the site (including downloaded deps)." \
    "-s" "${serve_site}" \
      "Instead of build and exit, serve the built site in dev mode." \
    "-i" "${include_ci_tool_docs}" \
      "Include subfolder with documentation about modules and commands." \
  );
  if (print_usage_and_help "${@:1}"); then return 1; fi;

  # Specific stage: Option parsing
  local opt="";
  local OPTIND=0;
  while getopts "${_getopts}" opt;
  do
    case ${opt} in
      "d") docs_folder="${OPTARG}"; ;;
      "t") tpl_folder="${OPTARG}"; ;;
      "o") out_folder="${OPTARG}"; ;;
      "c") full_clean="Yes"; ;;
      "s") serve_site="Yes"; ;;
      "i") include_ci_tool_docs="Yes"; ;;
    esac;
  done;
  shift "$((OPTIND-1))";

  # Specific stage: Path checks
  if (path_not_found "${docs_folder}"); then return 1; fi;
  if (path_not_found "${tpl_folder}"); then return 1; fi;

  # Remove full folder?
  if ([ "${full_clean}" = "Yes" ] && [ -d "${out_folder}" ]);
  then
    echo "Performing full clean. Deleting '${out_folder}'.";
    if (! rm -rf "${out_folder}");
    then
      echo "${AD_CI_APPNAME} ${FUNCNAME[0]}: Cannot remove '${out_folder}'.";
      return 1;
    fi;
  fi;

  # Remove 'docs/' if partial clean only
  echo "Performing docs clean. Deleting '${out_folder}/docs'.";
  rm -rf "${out_folder}/docs";

  # Create folder structure from template folder
  echo "Copying VuePress template content to build folder.";
  mkdir -p "${out_folder}";
  cp -vr "${tpl_folder}"/. "${out_folder}";

  # Do we have a valid folder?
  if (path_not_found "${out_folder}"); then return 1; fi;

  # Copy repository documentation content to VuePress 'docs' folder
  echo "Copying repository documentation to build folder.";
  mkdir -p "${out_folder}/docs";
  cp -vr "${docs_folder}"/. "${out_folder}/docs";

  # Do we have a valid folder?
  if (path_not_found "${out_folder}/docs"); then return 1; fi;

  if ([ "${include_ci_tool_docs}" = "Yes" ]);
  then
    # Create ci-tool folder for this documentation
    mkdir -p "${out_folder}/docs/ci-tool";

    # Create document for available modules to this script
    "${0}" -m > "${out_folder}/docs/ci-tool/modules-info.md";

    # Create document for available commands to this script
    "${0}" -c > "${out_folder}/docs/ci-tool/commands-info.md";
  fi;

  # Calculate baseUrl for VuePress
  local GITLAB_URL_REGEX="^([^\\/]+)(.*)$";
  local vuepress_baseurl="/";
  echo "VuePress baseUrl:";
  if [[ "${CI_PROJECT_PATH}" =~ ${GITLAB_URL_REGEX} ]];
  then
    echo "  + <prefix>.gitlab.io ..: ${BASH_REMATCH[1]}";
    echo "  + gitlab.io/<suffix> ..: ${BASH_REMATCH[2]}/";
    vuepress_baseurl="${BASH_REMATCH[2]}/";
  fi;
  echo "Computed baseUrl: ${vuepress_baseurl}.";

  # Create package.json from template
  export vuepress_baseurl;
  envsubst \
    < "${out_folder}/package.template.json" \
    > "${out_folder}/package.json";

  # Install VuePress dependencies
  cd "${out_folder}";
  echo "Installing dependencies.";
  yarn install || return $?;

  # Build or serve, depending the user choice
  if ([ "${serve_site}" = "Yes" ]);
  then
    echo "Build & serve VuePress in development mode";
    yarn docs:dev || return $?;
  else
    echo "Build VuePress in distribution mode";
    yarn docs:build || return $?;
  fi;

  # Return gracefully
  return 0;
};
