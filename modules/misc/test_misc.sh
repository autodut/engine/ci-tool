#!/usr/bin/printf ERROR: This file is a test library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Export only a variable containing a list of test cases. This list is composed
# by groups of consecutive values, described as follows:
#   - Test title: Human-readable short test describing the test
#   - Expected result: 0 for success, 1 for error
#   - Command line: Whatever is needed after 'ci-tool' command
export test_cases=( \
  # Command: build_git-changelog
  "*build_git-changelog:* Empty call" \
    "0" "build_git-changelog" \
  "*build_git-changelog:* Help request" \
    "1" "build_git-changelog -h" \
  # Command: build_release-notes
  "*build_release-notes:* Empty call" \
    "0" "build_release-notes" \
  "*build_release-notes:* Help request" \
    "1" "build_release-notes -h" \
  "*build_release-notes:* Invalid notes file" \
    "1" "build_release-notes -n an-invalid-notes-file" \
  "*build_release-notes:* Invalid changelog file" \
    "1" "build_release-notes -c an-invalid-changelog-file" \
  "*build_release-notes:* Invalid template file" \
    "1" "build_release-notes -t an-invalid-template-file" \
  # Command: build_badge
  "*build_badge:* Empty call" \
    "0" "build_badge" \
  "*build_badge:* Help request" \
    "1" "build_badge -h" \
  "*build_badge:* Invalid template file" \
    "1" "build_badge -t an-invalid-template-file" \
#  # Command: build_vuepress
#  "*build_vuepress:* Clean rebuild" \
#    "0" "build_vuepress -c" \
#  "*build_vuepress:* Empty call (Normal build)" \
#    "0" "build_vuepress" \
#  "*build_vuepress:* Help request" \
#    "1" "build_vuepress -h" \
#  "*build_vuepress:* Normal build + include dev docs" \
#    "0" "build_vuepress -i" \
);
