#!/usr/bin/printf ERROR: This file is a library. Exitting now ..%.s\n
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MIT License
# 
# Copyright (c) 2020 CieNTi <cienti@cienti.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# There is only one variable that is mandatory: AD_CI_TOOL_PATH
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if ([ "${AD_CI_TOOL_PATH}" = "" ] || [ ! -d "${AD_CI_TOOL_PATH}" ]);
then
  echo "${0} (${AD_CI_NAME}): ERROR! Invalid AD_CI_TOOL_PATH";
  echo "  - Current value .: '${AD_CI_TOOL_PATH}'";
  exit 1;
fi;

# Module 'core': It cannot be unselected as it is mandatory
AD_CI_MODULES=( "core" );

# Module 'gitflow'
AD_CI_GITFLOW=${AD_CI_GITFLOW:-"On"};
if ([ "${AD_CI_GITFLOW}" = "On" ]);
then
  AD_CI_MODULES+=( "gitflow" );
fi;

# Module 'gitlab'
AD_CI_GITLAB=${AD_CI_GITLAB:-"On"};
if ([ "${AD_CI_GITLAB}" = "On" ]);
then
  AD_CI_MODULES+=( "gitlab" );
fi;

# Module 'misc'
AD_CI_MISC=${AD_CI_MISC:-"On"};
if ([ "${AD_CI_MISC}" = "On" ]);
then
  AD_CI_MODULES+=( "misc" );
fi;
